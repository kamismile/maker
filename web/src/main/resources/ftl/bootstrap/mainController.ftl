package ${controllerPackage};

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author: ${Author}
 * @date: ${DATE}
 */
@Controller
public class MainController {
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
    public String login() throws Exception {
        return "login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/main")
    public String main() throws Exception {
        return "redirect:main";
    }

}