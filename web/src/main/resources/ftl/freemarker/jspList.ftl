<${r'#'}include "/macro/publicMacro.ftl">
<${r'#'}import "/macro/pagination.ftl" as Pagination>

<${r'@'}header title="${util.comment(table)}列表">
    <link href="/resource/common/css/pagination.css" rel="stylesheet">
    <script src="/resource/common/js/pagination.js"></script>
    <script src="/resource/common/js/jquery.validate.js"></script>
</${r'@'}header>

<${r'@'}body>
    <h2>${util.comment(table)}管理</h2>
    <div class="alert alert-light" role="alert">
        <span style="float:left"><a href="/${util.firstLower(table.javaName)}/toAdd">添加数据</a></span>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <#list table.columnList as column>
                <#if column.create>
                <th>${util.comment(column)}</th>
                </#if>
            </#list>
                <th>操作</th>
            </thead>
            <tbody>
            <${r'#'}list ${util.firstLower(table.javaName)}s.beans as ${util.firstLower(table.javaName)}>
                <tr>
                <#list table.columnList as column>
                <#if column.create>
                <#if column.foreign>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'!}'}</th>
                <#elseif util.isDate(column)>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${('}${util.firstLower(table.javaName)}.${column.property}?string('yyyy-MM-dd hh:mm:ss')${r')!}'} </th>
                <#elseif util.isNumber(column)>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</th>
                <#elseif util.isEnum(column)>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}.desc${r'!}'}</th>
                <#else>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</th>
                </#if>
                </#if>
                </#list>
                    <th><a href="/${util.firstLower(table.javaName)}/view/${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'?c}'}">详情</a>|<a href="/${util.firstLower(table.javaName)}/toEdit/${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'?c}'}">编辑</a></th>
                </tr>
            </${r'#'}list>
            </tbody>
        </table>

        <${r'@'}Pagination.pagination formId="${util.firstLower(table.javaName)}Form"
            pageNo=${util.firstLower(table.javaName)}s.pageNo
            pageTotal=${util.firstLower(table.javaName)}s.pageTotal
            total=${util.firstLower(table.javaName)}s.total
            pageSize=${util.firstLower(table.javaName)}s.pageSize
            action="/${util.firstLower(table.javaName)}/list">
        </${r'@'}Pagination.pagination>
    </div>
</${r'@'}body>

<${r'@'}footer>

</${r'@'}footer>