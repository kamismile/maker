package ${beanPackage};

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

/**
* @author: ${Author}
* @date: ${DATE}
*/
public class DbUtil {

    private static volatile JDBCClient jdbcClient = null;

    public static JDBCClient getJdbcClient(Vertx vertx) {
        if (jdbcClient == null) {
            synchronized (DbUtil.class) {
                if (jdbcClient == null) {
                    jdbcClient = createHikarCpJdbcClient(vertx);
                }
            }
        }
        return jdbcClient;
    }

    public static JDBCClient createDefaultJdbcClient(Vertx vertx) {
        return JDBCClient.createShared(vertx, new JsonObject()
            .put("driver_class", "com.mysql.jdbc.Driver")
            .put("url", "jdbc:mysql://${jdbc_ip}:${jdbc_port?c}/${jdbc_dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false")
            .put("user", "${jdbc_userName}")
            .put("password", "${jdbc_password}")
            .put("max_pool_size", 200)
            .put("castUUID", true));
    }

    public static JDBCClient createHikarCpJdbcClient(Vertx vertx) {
        return JDBCClient.createShared(vertx, new JsonObject()
            .put("provider_class", "io.vertx.ext.jdbc.spi.impl.HikariCPDataSourceProvider")
            .put("driver_class", "com.mysql.jdbc.Driver")
            .put("jdbcUrl", "jdbc:mysql://${jdbc_ip}:${jdbc_port?c}/${jdbc_dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false")
            .put("username", "${jdbc_userName}")
            .put("password", "${jdbc_password}")
            .put("maximumPoolSize", 200)
            .put("minimumIdle", 10)
            .put("cachePrepStmts", true)
            .put("prepStmtCacheSize", 250)
            .put("prepStmtCacheSqlLimit", 2048)
            .put("castUUID", true));
    }
}
