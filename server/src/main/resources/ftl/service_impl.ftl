package ${package};

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

/**
* @author: ${Author}
* @date: ${DATE}
*/
@Service("${service}")
public class ${className} <#if implements?exists>implements <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if>{

	@Resource
	private ${daoMaker.simpleName} ${util.javaName(daoMaker.simpleName)};
	
	<#------------------insert-------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insert") >
	@Override
	@Transactional
	public ${bean} insert(${bean} ${util.javaName(bean)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.insert(${util.javaName(bean)});
		if (n != 1) {
			throw new RuntimeException("${bean} insert error!");
		}
		return ${util.javaName(bean)};
	}
	
	</#if>

	<#------------------insertSelective-------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insertSelective") >
	@Override
	@Transactional
	public ${bean} insertSelective(${bean} ${util.javaName(bean)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.insertSelective(${util.javaName(bean)});
		if (n != 1) {
			throw new RuntimeException("${bean} insert error!");
		}
		return ${util.javaName(bean)};
	}

	</#if>
	<#------------------update-------------------------------------------------------------------------------------------->
	<#list table.indexies as index>
	<#if index.primaryKey>
	<#if util.isCreate(table, "updateBy${util.uniqueName(index)}") >
	@Override
	@Transactional
	public ${bean} updateBy${util.uniqueName(index)}(${bean} ${util.javaName(bean)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.updateBy${util.uniqueName(index)}(${util.javaName(bean)});
		if (n != 1) {
			throw new RuntimeException("${bean} update error!");
		}
		return ${util.javaName(bean)};
	}
	
	</#if>

	<#------------------update-------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "updateNotNullBy${util.uniqueName(index)}") >
	@Override
	@Transactional
	public ${bean} updateNotNullBy${util.uniqueName(index)}(${bean} ${util.javaName(bean)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.updateNotNullBy${util.uniqueName(index)}(${util.javaName(bean)});
		if (n != 1) {
			throw new RuntimeException("${bean} update error!");
		}
		return ${util.javaName(bean)};
	}
	</#if>

	<#------------------delete------------------------------------------------------------------------------------------>
	<#if util.isCreate(table, "deleteBy${util.uniqueName(index)}") >
	@Override
	@Transactional
	public void deleteBy${util.uniqueName(index)}(${util.uniqueParam(index)}) {
		int n = ${util.javaName(daoMaker.simpleName)}.deleteBy${util.uniqueName(index)}(${util.uniqueValue(index)});
		if (n != 1) {
			throw new RuntimeException("${bean} delete error!");
		}
	}

	</#if>
	</#if>
	</#list>
	<#-----------------------------------getUnique----------------------------------------------------------------------------------->
	<#list table.indexies as index>	
	<#if util.isUnique(index, table)>
	<#if util.isCreate(table, "getBy" + util.uniqueName(index))>
	<#if implements?exists>
	@Override
	</#if>
	public ${bean} getBy${util.uniqueName(index)}(${util.uniqueParam(index)}) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getBy${util.uniqueName(index)}(${util.uniqueValue(index)});
		return ${util.javaName(bean)};
    }
    </#if>
    </#if>
	</#list>
	<#------------------getManyToMany----------------------------------------------------------------------------------->
	<#if table.manyToManys??>
	<#list table.manyToManys as mm>	
	<#if util.isCreate(mm.primaryTable, "getMany" + util.firstUpper(mm.secondTable.javaName) + "sById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getMany${util.firstUpper(mm.secondTable.javaName)}sById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getMany${util.firstUpper(mm.secondTable.javaName)}sById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	</#if>
	<#------------------getWith----------------------------------------------------------------------------------------->
	<#list table.foreignKeys as foreignKey>	
	<#if util.isCreate(table, "getWith${util.firstUpper(foreignKey.column.property)}ById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getWith${util.firstUpper(foreignKey.column.property)}ById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getWith${util.firstUpper(foreignKey.column.property)}ById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	<#------------------getWithAssociatesById--------------------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getWithAssociatesById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getWithAssociatesById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getWithAssociatesById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#if>
	<#------------------getJoin----------------------------------------------------------------------------------------->
	<#list table.foreignKeys as foreignKey>	
	<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.column.property)}ById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getJoin${util.firstUpper(foreignKey.column.property)}ById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getJoin${util.firstUpper(foreignKey.column.property)}ById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	<#------------------getJoinAssociatesById--------------------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getJoinAssociatesById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getJoinAssociatesById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getJoinAssociatesById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#if>
	
	<#------------------getManyJoin------------------------------------------------------------------------------------->
	<#list table.many as foreignKey>	
	<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.table.javaName)}ById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getJoin${util.firstUpper(foreignKey.table.javaName)}ById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getJoin${util.firstUpper(foreignKey.table.javaName)}ById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#list>
	<#------------------getManyJoinAllById------------------------------------------------------------------------------>
	<#if table.many?size gt 1>
	<#if util.isCreate(table, "getJoinAllById") >
	@Override
	@Transactional(readOnly = true)
	public ${bean} getJoinAllById(${key} id) {
		${bean} ${util.javaName(bean)} = ${util.javaName(daoMaker.simpleName)}.getJoinAllById(id);
		return ${util.javaName(bean)};
	}
	
	</#if>
	</#if>
	<#------------------find-------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	@Override
	@Transactional(readOnly = true)
	public PageResult<${bean}> findBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)}) {
		long total = ${util.javaName(daoMaker.simpleName)}.findTotalBy${beanWhere}(${util.javaName(beanWhere)});
		List<${bean}> imageList = new ArrayList<${bean}>();
		if (total > 0) {
			imageList  = ${util.javaName(daoMaker.simpleName)}.findBy${beanWhere}(${util.javaName(beanWhere)});
		}
		
		return new PageResult<${bean}>(${util.javaName(beanWhere)}, total, imageList);
	}
	
	</#if>
	<#------------------findByJoin-------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "findByJoin") >
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	@Override
	@Transactional(readOnly = true)
	public PageResult<${bean}> findJoinBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)}) {
		long total = ${util.javaName(daoMaker.simpleName)}.findJoinTotalBy${beanWhere}(${util.javaName(beanWhere)});
		List<${bean}> imageList = new ArrayList<${bean}>();
		if (total > 0) {
			imageList  = ${util.javaName(daoMaker.simpleName)}.findJoinBy${beanWhere}(${util.javaName(beanWhere)});
		}
		
		return new PageResult<${bean}>(${util.javaName(beanWhere)}, total, imageList);
	}
	</#if>
	</#if>
	</#if>
	
}
