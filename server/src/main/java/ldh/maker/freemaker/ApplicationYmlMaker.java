package ldh.maker.freemaker;

import ldh.maker.vo.DBConnectionData;

/**
 * Created by ldh on 2017/4/8.
 */
public class ApplicationYmlMaker extends FreeMarkerMaker<ApplicationYmlMaker> {

    protected String projectRootPackage;
    protected String projectName;
    protected String dbName;
    protected DBConnectionData dBConnectionData;

    public ApplicationYmlMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public ApplicationYmlMaker projectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public ApplicationYmlMaker dbName(String dbName) {
        this.dbName = dbName;
        return this;
    }

    public ApplicationYmlMaker dBConnectionData(DBConnectionData dBConnectionData) {
        this.dBConnectionData = dBConnectionData;
        return this;
    }

    @Override
    public ApplicationYmlMaker make() {
        data();
        if (ftl == null) {
            this.out("application.ftl", data);
        } else {
            this.out(ftl, data);
        }
        return this;
    }

    @Override
    public void data() {
        fileName = "application.yml";
        data.put("projectRootPackage", projectRootPackage);
        data.put("projectName", projectName);
        data.put("dbConnectionData", dBConnectionData);
        data.put("dbName", dbName);
    }
}
