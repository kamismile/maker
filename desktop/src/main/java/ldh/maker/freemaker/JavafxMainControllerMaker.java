package ldh.maker.freemaker;

import ldh.database.Table;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxMainControllerMaker extends FreeMarkerMaker<JavafxMainControllerMaker> {

    protected Table table;
    protected String projectPackage;
    protected String pojoPackage;
    protected String servicePackage;
    protected String pack;
    protected String enumProjectPackage;
    protected String author;
    protected String description;

    public JavafxMainControllerMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxMainControllerMaker pack(String pack) {
        this.pack = pack;
        return this;
    }

    public JavafxMainControllerMaker servicePackage(String servicePackage) {
        this.servicePackage = servicePackage;
        return this;
    }

    public JavafxMainControllerMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    public JavafxMainControllerMaker pojoPackage(String pojoPackage) {
        this.pojoPackage = pojoPackage;
        return this;
    }

    public JavafxMainControllerMaker enumProjectPackage(String enumProjectPackage) {
        this.enumProjectPackage = enumProjectPackage;
        return this;
    }

    public JavafxMainControllerMaker author(String author) {
        this.author = author;
        return this;
    }

    public JavafxMainControllerMaker description(String description) {
        this.description = description;
        return this;
    }

    @Override
    public JavafxMainControllerMaker make() {
        data();
        out(ftl, data);
        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "MainController.java";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("pojoPackage", pojoPackage);
        data.put("controllerPackage", pack);
        data.put("servicePackage", servicePackage);
        data.put("enumProjectPackage", enumProjectPackage);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author", author);
        data.put("DATE", str);
        data.put("Description",description);
    }
}
