<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.shape.*?>
<?import javafx.scene.canvas.*?>
<?import javafx.geometry.*?>
<?import javafx.scene.control.*?>
<?import java.lang.*?>
<?import javafx.scene.layout.*?>

<?import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView?>
<?import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView?>
<VBox fx:id="leftPane" xmlns:fx="http://javafx.com/fxml"   fx:controller="${projectPackage}.controller.HomeLeftController">
    <opaqueInsets>
        <Insets bottom="10.0" left="10.0" right="10.0" top="10.0" />
    </opaqueInsets>
    <children>
        <#list tableInfo.tables?keys as key>
            <#if tableInfo.tables[key].create && !tableInfo.tables[key].middle >
            <#if key_index%6 == 0 && key_has_next>
                <#if key_index != 0>
                </VBox>
            </content>
        </TitledPane >
                </#if>
        <TitledPane text="Group管理" styleClass="leftItem" expanded="false">
            <graphic>
                <MaterialDesignIconView styleClass="ui-graphic"/>
            </graphic>
            <content>
                <VBox>
                    <Button text="${util.comment(tableInfo.tables[key])}管理" onAction="#leftNaveClick" userData="${tableInfo.tables[key].javaName}">
                        <graphic><FontAwesomeIconView styleClass="circle-graphic"/></graphic>
                    </Button>
            <#else>
                    <Button text="${util.comment(tableInfo.tables[key])}管理" onAction="#leftNaveClick" userData="${tableInfo.tables[key].javaName}">
                        <graphic><FontAwesomeIconView styleClass="circle-graphic"/></graphic>
                    </Button>
            </#if>
            </#if>
        </#list>
                </VBox>
            </content>
        </TitledPane >
    </children>
</VBox>

